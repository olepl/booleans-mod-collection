# Boolean's Mod Collection

## Installation

1. Install [Prism Launcher](https://prismlauncher.org/download/)
2. Click on `Add Instance`
3. Select `Import from zip`
4. Paste `https://gitlab.com/olepl/booleans-mod-collection/-/raw/41bb18d4a01b15c26b2f3bb3d59b7ba9ad8230e1/Template.zip`
5. Enjoy!
